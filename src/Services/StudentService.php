<?php

namespace App\Services;

use App\Repository\StudentRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class StudentService
{
    /**
     * @var StudentRepository
     */
    private $studentRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(StudentRepository $studentRepository, EntityManagerInterface $em)
    {
        $this->studentRepository = $studentRepository;
        $this->em = $em;
    }

    /**
     * @param $from
     * @param $to
     * @param $room
     */
    public function insertShiftStudent($from, $to, $room, $shift)
    {
        for($j = $from; $j <= $to; $j++) {

            //check if student already inserted
            $student = $this->studentRepository->findOneBy(['rollNo' => $j, 'room' => $room]);

            if(!$student) {
                //insert student
                $student = new \App\Entity\Student();
                $student->setName('Student '. $j);
                $student->setRollNo($j);
                $student->setExamShift($shift);
                $student->setRoom($room);
                $student->setCreatedBy(null);
                $student->setCreatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
                $student->setUpdatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

                $this->em->persist($student);
            }

        }

        $this->em->flush();
    }
}