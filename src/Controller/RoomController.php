<?php

namespace App\Controller;

use App\Entity\Room;
use App\Form\RoomType;
use App\Repository\RoomRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class RoomController extends AbstractController
{
    /**
     * @Route ("/admin/add_room", name="admin/add_room")
     */
    public function addRoom(Request $request
        , EntityManagerInterface $entityManager
        , Security $security

    ): Response{
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $roomForm = $this->createForm(RoomType::class);
        $roomForm->handleRequest($request);

        if($roomForm->isSubmitted() && $roomForm->isValid()){
            /** @var Room $room */
            $room = $roomForm->getData();

            $room->setCreatedBy($security->getUser());
            $room->setCreatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $room->setUpdatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

            $entityManager->persist($room);
            $entityManager->flush();

            return $this->redirectToRoute('admin/room_list');
        }
        return $this->render('admin/room/add.html.twig',[
                'roomForm' => $roomForm->createView()
            ]
        );
    }

    /**
     * @Route("/admin/room_list", name="admin/room_list")
     */
    public function roomList(RoomRepository $roomRepository){
        $roomList = $roomRepository->findAllRooms();
        return $this->render('admin/room/list.html.twig', [
            'roomList' => $roomList
        ]);
    }
}