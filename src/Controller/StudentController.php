<?php

namespace App\Controller;

use App\Entity\Building;
use App\Entity\Room;
use App\Entity\Student;
use App\Form\BulkUploadType;
use App\Form\RoomType;
use App\Form\StudentType;
use App\Repository\BuildingRepository;
use App\Repository\RoomRepository;
use App\Repository\StudentRepository;
use App\Services\StudentService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class StudentController extends AbstractController
{
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var StudentRepository
     */
    private $studentRepository;
    /**
     * @var StudentService
     */
    private $studentService;

    public function __construct(
        BuildingRepository $buildingRepository,
        RoomRepository $roomRepository,
        Security $security,
        EntityManagerInterface $em,
        StudentRepository $studentRepository,
        StudentService $studentService
    )
    {
        $this->buildingRepository = $buildingRepository;
        $this->roomRepository = $roomRepository;
        $this->security = $security;
        $this->em = $em;
        $this->studentRepository = $studentRepository;
        $this->studentService = $studentService;
    }

    /**
     * @Route ("/admin/add_student", name="admin/add_student")
     */
    public function addStudent(Request $request
        , EntityManagerInterface $entityManager
        , Security $security

    ): Response{
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $studentForm = $this->createForm(StudentType::class);
        $studentForm->handleRequest($request);

        if($studentForm->isSubmitted() && $studentForm->isValid()){
            /** @var Student $student */
            $student = $studentForm->getData();

            $student->setCreatedBy($security->getUser());
            $student->setCreatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $student->setUpdatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

            $entityManager->persist($student);
            $entityManager->flush();

            return $this->redirectToRoute('admin/student_list');
        }

        return $this->render('admin/student/add.html.twig',[
                'studentForm' => $studentForm->createView()
            ]
        );
    }

    /**
     * @Route("/admin/student_list", name="admin/student_list")
     */
    public function studentList(StudentRepository $studentRepository){
        $studentList = $studentRepository->findAllStudents();
        return $this->render('admin/student/list.html.twig', [
            'studentList' => $studentList
        ]);
    }

    /**
     * @Route("/admin/bulk_upload", name="admin/bulk_upload")
     */
    public function bulkUpload(Request $request)
    {
        $form = $this->createForm(BulkUploadType::class);
        $form->handleRequest($request);
        $studentInserted = 0;
        $todaysDate = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));

        if($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('csvFile')->getData();

            // Open the file
            if (($handle = fopen($file->getPathname(), "r")) !== false) {
                // Read and process the lines.
                $fullData = [];
                while (($data = fgetcsv($handle)) !== false) {
                    array_push($fullData, $data);
                }

                for($i = 1, $totalRow = count($fullData); $i < $totalRow; $i++) {

                    //find building information
                    $building = $this->buildingRepository->findOneBy(['name' => $fullData[$i][0]]);
                    if(!$building){
                        $building = new Building();
                        $building->setName($fullData[$i][0]);
                        $building->setCreatedAt($todaysDate);
                        $building->setUpdatedAt($todaysDate);

                        $this->em->persist($building);
                        $this->em->flush();
                    }

                    //find room information
                    $room = $this->roomRepository->findOneBy(['roomNo' => $fullData[$i][2], 'building' => $building]);

                    //if not found, create room
                    if(!$room) {
                        $room = new Room();
                        $room->setBuilding($building);
                        $room->setRoomNo($fullData[$i][2]);
                        $room->setCreatedBy($this->security->getUser());
                        $room->setCreatedAt($todaysDate);
                        $room->setUpdatedAt($todaysDate);

                        $this->em->persist($room);
                        $this->em->flush();
                    }

                    //insert individual student from range of student
                    //shift1
                    $this->studentService->insertShiftStudent($fullData[$i][5], $fullData[$i][6], $room, '1st Shift');

                    //shift2
                    $this->studentService->insertShiftStudent($fullData[$i][7], $fullData[$i][8], $room, '2nd Shift');

                    //shift3
                    $this->studentService->insertShiftStudent($fullData[$i][9], $fullData[$i][10], $room, '3rd Shift');

                    //shift4
                    $this->studentService->insertShiftStudent($fullData[$i][11], $fullData[$i][12], $room, '4th Shift');

                    //shift5
                    $this->studentService->insertShiftStudent($fullData[$i][13], $fullData[$i][14], $room, '5th Shift');

                    //shift6
                    $this->studentService->insertShiftStudent($fullData[$i][15], $fullData[$i][16], $room, '6th Shift');

                    //shift7
                    $this->studentService->insertShiftStudent($fullData[$i][17], $fullData[$i][18], $room, '7th Shift');

                    //shift8
                    $this->studentService->insertShiftStudent($fullData[$i][19], $fullData[$i][20], $room, '8th Shift');

                    //shift9
                    $this->studentService->insertShiftStudent($fullData[$i][21], $fullData[$i][22], $room, '9th Shift');

                }

                fclose($handle);
                $studentInserted = 1;
            }
        }

        return $this->render('admin/student/bulk_upload.html.twig',[
                'form' => $form->createView(),
                'studentInserted' => $studentInserted,
            ]
        );
    }
}