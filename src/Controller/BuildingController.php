<?php

namespace App\Controller;

use App\Entity\Building;
use App\Form\BuildingType;
use App\Repository\BuildingRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BuildingController extends AbstractController
{
    /**
     * @Route ("/admin/add_building", name="admin/add_building")
     */
    public function addBuilding(Request $request, EntityManagerInterface $entityManager){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $buildingForm = $this->createForm(BuildingType::class);
        $buildingForm->handleRequest($request);

        if($buildingForm->isSubmitted() && $buildingForm->isValid()){
            /** @var Building $building */
            $building = $buildingForm->getData();

            $building->setCreatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $building->setUpdatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

            $entityManager->persist($building);
            $entityManager->flush();

            return $this->redirectToRoute('admin/building_list');
        }
        return $this->render('admin/building/add.html.twig',[
                'buildingForm' => $buildingForm->createView()
            ]
        );
    }

    /**
     * @Route("/admin/building_list", name="admin/building_list")
     */
    public function buildingList(BuildingRepository $buildingRepository){
        $buildingList = $buildingRepository->findAll();
        return $this->render('admin/building/list.html.twig', [
            'buildingList' => $buildingList
        ]);
    }
}