<?php

namespace App\Controller;

use App\Form\SeatSearchType;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var StudentRepository
     */
    private $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * @Route("/", name="app_homepage")
     */
    public function appHome(Request $request)
    {
        $latitude = 23.8805432;
        $longitude = 90.2686082;
        $location = "Dept. of Public Health, Jahangirnagar University";
        $studentFound = 0;
        $studentInfo = [];

        $seatSearchForm = $this->createForm(SeatSearchType::class);
        $seatSearchForm->handleRequest($request);

        if($seatSearchForm->isSubmitted() && $seatSearchForm->isValid()) {

            $rollNumber = $seatSearchForm->getData()['roll_number'];
            $studentDetails = $this->studentRepository->findOneBy(['rollNo' => $rollNumber]);

            if($studentDetails) {
                $studentFound = 1;

                $studentInfo['rollNumber'] = $rollNumber;
                $studentInfo['name'] = $studentDetails->getName();
                $studentInfo['department'] = $studentDetails->getRoom()->getBuilding()->getName();
                $studentInfo['roomName'] = $studentDetails->getRoom()->getRoomNo();
                $studentInfo['shift'] = $studentDetails->getExamShift();
                $location = $studentDetails->getRoom()->getBuilding()->getName().', Jahangirnagar University';
            }
        }

        return $this->render('home/index.html.twig', [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'location' => $location,
            'seatSearchForm' => $seatSearchForm->createView(),
            'studentInfo' => $studentInfo,
            'studentFound' => $studentFound
        ]);
    }

    public function findShiftName($shiftNumber){
        switch ($shiftNumber){
            case 1:
                return '1st Shift';
            case 2:
                return '2nd Shift';
            case 3:
                return '3rd Shift';
            case 4:
                return '4th Shift';
            case 5:
                return '5th Shift';
            default:
                return '';
        }
    }
}
