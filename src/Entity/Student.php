<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 * @UniqueEntity(fields={"rollNo"}, message="You can't add same roll twice")
 */
class Student
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="students")
     */
    private $room;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="students")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $examShift;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rollNo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getExamShift(): ?string
    {
        return $this->examShift;
    }

    public function setExamShift(string $examShift): self
    {
        $this->examShift = $examShift;

        return $this;
    }

    public function getRollNo(): ?string
    {
        return $this->rollNo;
    }

    public function setRollNo(string $rollNo): self
    {
        $this->rollNo = $rollNo;

        return $this;
    }
}
