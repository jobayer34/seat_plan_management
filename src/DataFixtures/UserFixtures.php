<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;


class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;

    }

    public function load(ObjectManager $manager)
    {
        /** @var PasswordAuthenticatedUserInterface $user */
        $user = new User();
        $user->setEmail('jobayercse@gmail.com');
        $user->setRoles(['ROLE_ADMIN']);

        $user->setPassword($this->passwordEncoder->hashPassword(
            $user,
            '123456'
        ));

        //$user->setCreatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        //$user->setUpdatedAt(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

        $manager->persist($user);
        $manager->flush();

    }
}
